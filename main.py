import requests

uri = 'https://api.ipify.org?format=json'

try:
    res = requests.get(uri, timeout=5)
    msg = res.json()['ip']
except Exception:
    msg = 'Не удалось получить IP-адрес'


print('*' * 37)
print('Hello! This is a test project message')
print(f'Your IP: {msg}')
print('*' * 37)
