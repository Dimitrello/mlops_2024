# Курс MLOps 2024

Курс "MLOps и production в DS исследованиях 3.0" проводится на платформе ods.ai

## Описание
Описание раздела будет пополняться по мере выполнения домашних работ, новых идей, подходов к ведению репозитория
и вдохновения.

## Методология разработки (Git workflow)
Методология разработки в домашних заданиях - Github flow. Для обновления кода репозитория создаётся новая ветка,
содержащая feature/bugfix/enhancement commit, далее ветка вливается в master.

Методология для командного проекта ещё не определена, так как проект ещё обсуждается.



# Домашние задания 
В данном блоке приводится описания домашних заданий и реализация проделанной работы

### ДЗ-1
Необходимо создать репозиторий в Gitlab, подобрать линтеры и форматеры, провести их настройку 
в pyproject.toml и прописать их основные параметры, создать файлы CONTRIBUTING.md и README.md, где прописать
как пользоваться линтерами и описать методологию ведения проекта. Далее отправить ветку в Gitlab.

Был создан репозиторий на Gitlab. Выбран `ruff`, который включает функционал лиентера и форматера, произведены
настройки. Добавлен `pre-commit` для `git` и добавлены необходимые хуки в `pre-commit-config.yaml`. Внесено необходимое 
описание в файлы `CONTRIBUTING.md` и `README.md`.  

### ДЗ-2
Нужно зафиксировать зависимости с помощью одного из пакетных менеджеров, составить Dockerfile с построением окружения 
и прописать команды по его сборки и запуска в README.md. Желательно разделить зависимости по группам/окружениям. 

В качестве пакетного менеджера и менеджера зависимостей выбран `pdm`. Зафиксированы зависимости, настроен файл 
`pyproject.toml`. В `pyproject.toml` вынесена отдельная секция `[tool.pdm.dev-dependencies]`, содержащая
группу/окружение с названием `dev`. Опцию `[project.optional-dependencies]` пока решено не использовать, хотя её легко
вызвать стандартным менеджером `pip`. Собран и протестирован вручную `Dockerfile`.