FROM python:3.11-slim

WORKDIR /app

# install PDM
RUN pip install --no-cache-dir -U pdm

# disable update check
ENV PDM_CHECK_UPDATE=false

# copy files
COPY pyproject.toml pdm.lock README.md ./
COPY src/ ./src/

# install dependencies and project into the local packages directory
RUN pdm install --frozen-lockfile --no-editable

COPY . .

CMD ["pdm", "run", "main.py"]

